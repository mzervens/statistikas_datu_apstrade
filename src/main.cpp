#include <iostream>
#include <string>
#include "Parser.cpp"
using namespace std;

void wait()
{
	string s;
	cin >> s;

	return;
}

int main()
{
	Parser p;
	p.printAverageAge();
	p.printMedians();
	p.printDispersion();
	p.printStdDeviation();
	p.printAVGDeviation();
	p.printClassRelativeCummulativeFreq(dtaInx::TOTAL);

	wait();
	return 0;
}