#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
using namespace std;

enum dtaInx
{
	AGE = 0,
	TOTAL = 1,
	MEN = 2,
	FEMALE = 3
};


class Parser
{
	fstream f;
	vector<vector<int>> data;

	float avarageTotalAge;
	float averageMenAge;
	float averageFemaleAge;

	int ageTmsCntTot;
	int ageTmsCntMen;
	int ageTmsCntFem;

	int totalPeople;
	int totalMen;
	int totalFemales;


	float menMedian;
	float femaleMedian;
	float peopleMedian;

	float menDispersion;
	float femDispersion;
	float peopleDispersion;

	float menStdDeviation;
	float femStdDeviation;
	float peopleStdDeviation;

	float menAVGDeviation;
	float femAVGDeviation;
	float peopleAVGDeviation;


	vector<pair<int, int>> classes;
	vector<pair<int, pair<int, int>>> menFrequencies;
	vector<pair<int, pair<int, int>>> femaleFrequencies;
	vector<pair<int, pair<int, int>>> peopleFrequencies;


	void loadDtaInMemory()
	{
		int rowAge, rowTotal, rowMen, rowFemale;
		vector<int> row;
		f.seekg(0, ios::beg);

		data = vector<vector<int>>();

		while (!f.eof())
		{
			row = vector<int>(4);
			f >> rowAge;
			f >> rowTotal; 
			f >> rowMen;
			f >> rowFemale;

			row[dtaInx::AGE] = rowAge;
			row[dtaInx::FEMALE] = rowFemale;
			row[dtaInx::MEN] = rowMen;
			row[dtaInx::TOTAL] = rowTotal;

			data.push_back(row);

			rowAge = rowTotal = rowMen = rowFemale = 0;
		}

		return;
	}

	void calcAverageAge()
	{
		ageTmsCntTot = 0;
		ageTmsCntMen = 0;
		ageTmsCntFem = 0;

		for (int i = 0; i < data.size(); i++)
		{
			ageTmsCntTot += data[i][dtaInx::AGE] * data[i][dtaInx::TOTAL];
			ageTmsCntMen += data[i][dtaInx::AGE] * data[i][dtaInx::MEN];
			ageTmsCntFem += data[i][dtaInx::AGE] * data[i][dtaInx::FEMALE];

			totalPeople += data[i][dtaInx::TOTAL];
			totalMen += data[i][dtaInx::MEN];
			totalFemales += data[i][dtaInx::FEMALE];
		}

		avarageTotalAge = (float)ageTmsCntTot / totalPeople;
		averageMenAge = (float)ageTmsCntMen / totalMen;
		averageFemaleAge = (float)ageTmsCntFem / totalFemales;

	}

	void findMedians()
	{
		int index1;
		int index2;

		int med1;
		int med2;

		float med;

		int currTotalEl;
		dtaInx currIndx;

		float * targt;

		for (int i = 0; i < 3; i++)
		{
			if (i == 0)
			{
				currTotalEl = totalMen;
				currIndx = dtaInx::MEN;
				targt = &menMedian;
			}
			else if (i == 1)
			{
				currTotalEl = totalFemales;
				currIndx = dtaInx::FEMALE;
				targt = &femaleMedian;
			}
			else
			{
				currTotalEl = totalPeople;
				currIndx = dtaInx::TOTAL;
				targt = &peopleMedian;
			}

			if ((currTotalEl % 2) == 0)
			{
				index1 = currTotalEl / 2;
				index2 = index1 + 1;
			}
			else
			{
				index1 = ceil((float)currTotalEl / 2);
				index2 = -1;
			}

			for (int n = 0; n < data.size(); n++)
			{
				if (data[n][currIndx] < index1)
				{
					index1 -= data[n][currIndx];
					continue;
				}
				else if (data[n][currIndx] == index1)
				{
					med1 = data[n][dtaInx::AGE];
					if (index2 != -1)
					{
						med2 = data[n + 1][dtaInx::AGE];
					}
					break;
				}
				else if (data[n][currIndx] > index1)
				{
					med1 = data[n][dtaInx::AGE];
					if (index2 != -1)
					{
						med2 = data[n][dtaInx::AGE];
					}
					break;
				}

			}

			if (index2 != -1)
			{
				med = (float)(med1 + med2) / 2;
			}
			else
			{
				med = med1;
			}

			*targt = med;

		}


	}

	void calcDispersion()
	{

		float dispMenTop = 0;
		float dispFemTop = 0;
		float dispPeopTop = 0;

		float femRes;
		float peopRes;
		float menRes;

		for (int i = 0; i < data.size(); i++)
		{
			menRes = data[i][dtaInx::AGE] - averageMenAge;
			menRes *= menRes;
			dispMenTop += menRes * data[i][dtaInx::MEN];
			

			femRes = data[i][dtaInx::AGE] - averageFemaleAge;
			femRes *= femRes;
			dispFemTop += femRes * data[i][dtaInx::FEMALE];

			peopRes = data[i][dtaInx::AGE] - avarageTotalAge;
			peopRes *= peopRes;
			dispPeopTop += peopRes * data[i][dtaInx::TOTAL];
			
		}

		cout << "here " << dispMenTop << " " << dispFemTop << " " << dispPeopTop << endl;

		menDispersion = (float)dispMenTop / totalMen;
		femDispersion = (float)dispFemTop / totalFemales;
		peopleDispersion = (float)dispPeopTop / totalPeople;

	}

	void calcStandardDeviation()
	{
		menStdDeviation = sqrt(menDispersion);
		femStdDeviation = sqrt(femDispersion);
		peopleStdDeviation = sqrt(peopleDispersion);

	}

	void calcAverageDeviation()
	{
		float oneUnitDevMen;
		float oneUnitDevFem;
		float oneUnitDevPeopl;

		float topAVGDevMen = 0;
		float topAVGDevFem = 0;
		float topAVGDevPeopl = 0;

		for (int i = 0; i < data.size(); i++)
		{
			oneUnitDevMen = abs(data[i][dtaInx::AGE] - averageMenAge);
			oneUnitDevFem = abs(data[i][dtaInx::AGE] - averageFemaleAge);
			oneUnitDevPeopl = abs(data[i][dtaInx::AGE] - avarageTotalAge);

			topAVGDevMen += oneUnitDevMen * data[i][dtaInx::MEN];
			topAVGDevFem += oneUnitDevFem * data[i][dtaInx::FEMALE];
			topAVGDevPeopl += oneUnitDevPeopl * data[i][dtaInx::TOTAL];
		}

		menAVGDeviation = (float)topAVGDevMen / totalMen;
		femAVGDeviation = (float)topAVGDevFem / totalFemales;
		peopleAVGDeviation = (float)topAVGDevPeopl / totalPeople;
	}

	void calcClassFrequencies()
	{
		menFrequencies = vector<pair<int, pair<int, int>>>();
		femaleFrequencies = vector<pair<int, pair<int, int>>>();
		peopleFrequencies = vector<pair<int, pair<int, int>>>();


		for (int i = 0; i < classes.size(); i++)
		{
			pair<int, int> currClass = classes[i];
			int menFreq = 0;
			int femFreq = 0;
			int peopleFreq = 0;

			for (int n = 0; n < data.size(); n++)
			{
				if (data[n][dtaInx::AGE] > currClass.second)
				{
					break;
				}
				else if (data[n][dtaInx::AGE] < currClass.first)
				{
					continue;
				}
				else
				{
					menFreq += data[n][dtaInx::MEN];
					femFreq += data[n][dtaInx::FEMALE];
					peopleFreq += data[n][dtaInx::TOTAL];
				}

			}

			menFrequencies.push_back(pair<int, pair<int, int>>({ menFreq, currClass }));
			femaleFrequencies.push_back(pair<int, pair<int, int>>({ femFreq, currClass }));
			peopleFrequencies.push_back(pair<int, pair<int, int>>({ peopleFreq, currClass }));

		}

	}

	void createTestFile()
	{
		fstream o;
		o.open("test.txt", ios::out);

		for (int i = 0; i < data.size(); i++)
		{
			for (int n = 0; n < data[i][dtaInx::FEMALE]; n++)
			{
				o << data[i][dtaInx::AGE] << endl;
			}
		}

		o.close();
	}

public:
	Parser(string inputFile = "DatiPlain.txt")
	{
		f.open(inputFile.c_str(), ios::in);
		if (!f.is_open())
		{
			cout << "Couldnt open input file " << inputFile << endl;
			return;
		}

		cout <<  setprecision(17);


		classes = vector<pair<int, int>>
		({
			pair<int, int>({0, 9}),
			pair<int, int>({10, 19}),
			pair<int, int>({20, 29}),
			pair<int, int>({30, 39}),
			pair<int, int>({40, 49}),
			pair<int, int>({50, 59}),
			pair<int, int>({60, 69}),
			pair<int, int>({70, 79}),
			pair<int, int>({80, 89}),
			pair<int, int>({90, 100}),
		});


		totalMen = totalFemales = totalPeople = 0;

		loadDtaInMemory();
		calcAverageAge();
		findMedians();
		calcDispersion();
		calcStandardDeviation();
		calcAverageDeviation();
		calcClassFrequencies();

	//	createTestFile();
	}

	~Parser()
	{
		f.close();
	}

	void printAverageAge()
	{
		cout << "Average mens age is " << ageTmsCntMen << " / " << totalMen << " = " << averageMenAge << endl;
		cout << "Average females age is " << ageTmsCntFem << " / " << totalFemales << " = " << averageFemaleAge << endl;
		cout << "Average all peoples age is " << ageTmsCntTot << " / " << totalPeople << " = " << avarageTotalAge << endl;

		cout << endl;
	}

	void printMedians()
	{
		cout << "Mens age median is " << menMedian << endl;
		cout << "Females age median is " << femaleMedian << endl;
		cout << "People age median is " << peopleMedian << endl;

		cout << endl;
	}

	void printDispersion()
	{
		cout << "Mens age dispersion is " << menDispersion << endl;
		cout << "Females age dispersion is " << femDispersion << endl;
		cout << "People age dispaersion is " << peopleDispersion << endl;

		cout << endl;
	}

	void printStdDeviation()
	{
		cout << "Mens age standard deviation is " << menStdDeviation << endl;
		cout << "Females age standard deviation is " << femStdDeviation << endl;
		cout << "People age standard deviation is " << peopleStdDeviation << endl;

		cout << endl;
	}

	void printAVGDeviation()
	{
		cout << "Mens age average absolute deviation is " << menAVGDeviation << endl;
		cout << "Females age average absolute deviation is " << femAVGDeviation << endl;
		cout << "People age average absolute deviation is " << peopleAVGDeviation << endl;

		cout << endl;
	}


	void printClassFreq(dtaInx type, bool relative = false)
	{
		vector<pair<int, pair<int, int>>> * dta;
		int * total;

		if (type == dtaInx::MEN)
		{
			dta = &menFrequencies;
			cout << "Men class frequencies:" << endl;
			total = &totalMen;
		}
		else if (type == dtaInx::FEMALE)
		{
			dta = &femaleFrequencies;
			cout << "Female class frequencies:" << endl;
			total = &totalFemales;
		}
		else
		{
			dta = &peopleFrequencies;
			cout << "People class frequencies:" << endl;
			total = &totalPeople;
		}

		for (int i = 0; i < (*dta).size(); i++)
		{
			cout << "[" << (*dta)[i].second.first << " " << (*dta)[i].second.second << "] ";
			if (!relative)
			{
				cout << (*dta)[i].first << endl;
			}
			else
			{
				cout << ((float)(*dta)[i].first / *total) << endl;
			}
		}

	}

	void printClassCummulativeFreq(dtaInx type, bool relative = false)
	{
		vector<pair<int, pair<int, int>>> * dta;
		int * total;

		if (type == dtaInx::MEN)
		{
			dta = &menFrequencies;
			cout << "Men class cummulative frequencies:" << endl;
			total = &totalMen;
		}
		else if (type == dtaInx::FEMALE)
		{
			dta = &femaleFrequencies;
			cout << "Female class cummulative frequencies:" << endl;
			total = &totalFemales;
		}
		else
		{
			dta = &peopleFrequencies;
			cout << "People class cummulative frequencies:" << endl;
			total = &totalPeople;
		}

		for (int i = 0; i < (*dta).size(); i++)
		{
			cout << "[" << (*dta)[i].second.first << " " << (*dta)[i].second.second << "] ";
			int sum = 0;

			for (int n = 0; n <= i; n++)
			{
				sum += (*dta)[n].first;
			}

			if (!relative)
			{
				cout << sum << endl;
			}
			else
			{
				cout << ((float)sum / *total) << endl;
			}
		}
	}

	void printClassRelativeFreq(dtaInx type)
	{
		printClassFreq(type, true);
	}

	void printClassRelativeCummulativeFreq(dtaInx type)
	{
		printClassCummulativeFreq(type, true);
	}

};